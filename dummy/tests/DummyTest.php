<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\Dummy;

use FlyingAnvil\PunitTools\SlowTest\Attributes\SlowThreshold;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\PunitTools\Dummy\Dummy
 */
#[SlowThreshold(150)]
class DummyTest extends TestCase
{
    private Dummy $dummy;

    protected function setUp(): void
    {
        $this->dummy = new Dummy();
    }

//    public function testAddition(): void
//    {
//        self::assertSame(2, $this->dummy->add(1, 1));
//    }

//    #[SlowThreshold(111)]
    public function testAdditionWithSleep(): void
    {
        usleep(random_int(1_000_000, 1_200_000));
        self::assertSame(2, $this->dummy->add(1, 1));
    }

    /**
     * @dataProvider provider
     */
    #[SlowThreshold(11)]
    public function testAdditionWithProvider(int $sleep): void
    {
        usleep($sleep);
        self::assertSame(2, $this->dummy->add(1, 1));
    }

    public function provider(): iterable
    {
        yield [random_int(100_000, 200_000), 1, [2], fn() => null, new \stdClass(), 'foo, bar'];

        for ($i = 0; $i < 15; $i++) {
//            yield [(int)(100_000 * (($i * .1) + 1))];
            yield [random_int(100_000, 200_000)];
        }
    }
}
