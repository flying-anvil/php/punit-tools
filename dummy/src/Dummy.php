<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\Dummy;

class Dummy
{
    public function add(int $a, int $b): int
    {
        return $a + $b;
    }
}
