<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools;

use FlyingAnvil\PunitTools\Common\DataObject\TestInfo;
use FlyingAnvil\PunitTools\Common\Exception\ConfigurationException;
use FlyingAnvil\PunitTools\Exception\SlowTestException;
use FlyingAnvil\PunitTools\SlowTest\Attributes\SlowThreshold;
use FlyingAnvil\PunitTools\SlowTest\DataObject\SlowTestInfo;
use FlyingAnvil\PunitTools\SlowTest\ReportFormatter\DefaultReportFormatter;
use FlyingAnvil\PunitTools\SlowTest\ReportFormatter\ReportFormatterInterface;
use FlyingAnvil\PunitTools\SlowTest\SlowReport;
use FlyingAnvil\PunitTools\SlowTest\SlowTestOptions;
use PHPUnit\Runner\AfterLastTestHook;
use PHPUnit\Runner\AfterTestHook;
use ReflectionClass;
use ReflectionException;

class SlowTestReport implements AfterTestHook, AfterLastTestHook
{
    private const DEFAULT_SLOW_THRESHOLD = 100;
    private const DEFAULT_REPORT_LENGTH  = 10;

    private const REPORT_FORMATTER_MAPPING = [
        'default' => DefaultReportFormatter::class,
    ];

    private ReportFormatterInterface $reportFormatter;
    private SlowTestOptions $options;
    private SlowReport $report;

    public function __construct(
        private int $defaultSlowThreshold = self::DEFAULT_SLOW_THRESHOLD,
        int         $reportLength = self::DEFAULT_REPORT_LENGTH,
        string      $rawReportFormatter = 'default',
        array       $additionalOptions = [],
    ) {
        $this->reportFormatter = $this->getFormatter($rawReportFormatter);

        $useColors = match($additionalOptions['useColors'] ?? 'auto') {
            'always', 'true', 'yes', '1' => true,
            'never', 'false', 'no', '0'  => false,
            'auto'                       => stream_isatty(STDOUT),
            default => throw new ConfigurationException(
                self::class,
                'useColors',
                sprintf(
                    'must be one of (always|never|auto), "%s" given',
                    $additionalOptions['useColors'],
                ),
            ),
        };

        unset($additionalOptions['useColors']);

        $this->report = SlowReport::createEmpty();
        $this->options = SlowTestOptions::create(
            $defaultSlowThreshold,
            $reportLength,
            $useColors,
            $additionalOptions,
        );
    }

    private function getFormatter(string $rawReportFormatter): ReportFormatterInterface
    {
        if (class_exists($rawReportFormatter, true)) {
            if (!is_subclass_of($rawReportFormatter, ReportFormatterInterface::class, true)) {
                throw new ConfigurationException(
                    self::class,
                    $rawReportFormatter,
                    sprintf(
                        'must implement %s',
                        ReportFormatterInterface::class,
                    ),
                );
            }

            return new $rawReportFormatter();
        }

        if (!isset(self::REPORT_FORMATTER_MAPPING[$rawReportFormatter])) {
            throw new ConfigurationException(
                self::class,
                $rawReportFormatter,
                'Formatter does not exist. It\'s either not a built-in formatter or the class does not exist',
            );
        }

        $formatterClass = self::REPORT_FORMATTER_MAPPING[$rawReportFormatter];
        return new $formatterClass();
    }

    public function executeAfterTest(string $test, float $time): void
    {
        try {
            $testInfo  = TestInfo::createFromRaw($test);
            $threshold = $this->getTestSlowThreshold($testInfo);
        } catch (SlowTestException|ReflectionException) {
            // Ignore possible exceptions, as they indicate an error outside our control
            return;
        }

        $slowInfo = SlowTestInfo::create($testInfo, $time, $threshold);
        if ($slowInfo->getTestDuration() >= $threshold) {
            $this->report->addSlowTest($slowInfo);
        }
    }

    public function executeAfterLastTest(): void
    {
        $this->reportFormatter->format($this->report, $this->options);
    }

    /**
     * @throws ReflectionException
     */
    private function getTestSlowThreshold(TestInfo $testInfo): int
    {
        $reflectionClass = new ReflectionClass($testInfo->getFullTestClass());
        $reflectionMethod = $reflectionClass->getMethod($testInfo->getTestName());

        // Test method threshold
        $methodAttributes = $reflectionMethod->getAttributes(SlowThreshold::class);
        if (!empty($methodAttributes)) {
            /** @var SlowThreshold $attribute */
            $attribute = $methodAttributes[0]->newInstance();
            return $attribute->getSlowthreshold();
        }

        // Test class threshold
        $classAttributes = $reflectionClass->getAttributes(SlowThreshold::class);
        if (!empty($classAttributes)) {
            /** @var SlowThreshold $attribute */
            $attribute = $classAttributes[0]->newInstance();
            return $attribute->getSlowthreshold();
        }

        // Use default if not overridden
        return $this->defaultSlowThreshold;
    }
}
