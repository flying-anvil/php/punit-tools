<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\Exception;

class SlowTestException extends PunitToolsException
{
}
