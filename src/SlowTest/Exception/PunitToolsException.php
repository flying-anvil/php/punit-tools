<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\Exception;

use FlyingAnvil\Libfa\Exception\FlyingAnvilException;

class PunitToolsException extends FlyingAnvilException
{
}
