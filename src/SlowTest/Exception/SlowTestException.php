<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\Exception;

class SlowTestException extends PunitToolsException
{
}
