<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\DataObject;

use FlyingAnvil\PunitTools\Common\DataObject\TestInfo;

class SlowTestInfo
{
    public function __construct(
        private TestInfo $testInfo,
        private float $testDuration,
        private float $slowThreshold,
    ) {}

    public static function create(TestInfo $testInfo, float $testDurationSeconds, int $slowThreshold): self
    {
        $testDuration = $testDurationSeconds * 1e3;
        return new self(
            $testInfo,
            $testDuration,
            $slowThreshold,
        );
    }

    /**
     * @return float In milliseconds
     */
    public function getTestDuration(): float
    {
        return $this->testDuration;
    }

    public function getSlowThreshold(): float
    {
        return $this->slowThreshold;
    }

    public function getFullTestClass(): string
    {
        return $this->testInfo->getFullTestClass();
    }

    public function getShortTestClass(): string
    {
        return $this->testInfo->getShortTestClass();
    }

    public function getTestName(): string
    {
        return $this->testInfo->getTestName();
    }

    public function hasDataSet(): bool
    {
        return $this->testInfo->hasDataSet();
    }

    public function getDataSetNumber(): ?int
    {
        return $this->testInfo->getDataSetNumber();
    }

    public function getDataSetDescription(): ?string
    {
        return $this->testInfo->getDataSetDescription();
    }

    public function toString(): string
    {
        return $this->testInfo->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
