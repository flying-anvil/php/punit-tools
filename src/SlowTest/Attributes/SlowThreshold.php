<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_CLASS)]
class SlowThreshold
{
    public function __construct(
        private int $slowthreshold
    ) {}

    public function getSlowthreshold(): int
    {
        return $this->slowthreshold;
    }
}
