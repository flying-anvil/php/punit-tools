<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest;

class SlowTestOptions
{
    private function __construct(
        private int $defaultSlowThreshold,
        private int $reportLength,
        private bool $useColors,
        private array $additionalOptions = [],
    ) {}

    public static function create(
        int $defaultSlowThreshold,
        int $reportLength,
        bool $useColors,
        array $additionalOptions = [],
    ): self {
        return new self($defaultSlowThreshold, $reportLength, $useColors, $additionalOptions);
    }

    public function getDefaultSlowThreshold(): int
    {
        return $this->defaultSlowThreshold;
    }

    public function getReportLength(): int
    {
        return $this->reportLength;
    }

    public function getUseColors(): ?bool
    {
        return $this->useColors;
    }

    public function getAdditionalOptions(): array
    {
        return $this->additionalOptions;
    }
}
