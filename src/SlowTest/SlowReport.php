<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest;

use Countable;
use FlyingAnvil\PunitTools\SlowTest\DataObject\SlowTestInfo;

class SlowReport implements Countable
{
    /** @var SlowTestInfo[] */
    private array $slowTests = [];

    private function __construct() {}

    public static function createEmpty(): self
    {
        return new self();
    }

    public function addSlowTest(SlowTestInfo $testInfo): void
    {
        $this->slowTests[] = $testInfo;
    }

    public function getSlowTests(): array
    {
        return $this->slowTests;
    }

    /**
     * @return SlowTestInfo[] Slowest test is first
     */
    public function getSlowTestsSorted(): array
    {
        $sorted = $this->slowTests;
        usort(
            $sorted,
            static fn(SlowTestInfo $left, SlowTestInfo $right) => $right->getTestDuration() <=> $left->getTestDuration(),
        );

        return $sorted;
    }

    public function getSlowestTest(): ?SlowTestInfo
    {
        if (empty($this->slowTests)) {
            return null;
        }

        $slowest = $this->slowTests[0];

        foreach ($this->slowTests as $test) {
            if ($test->getTestDuration() > $slowest->getTestDuration()) {
                $slowest = $test;
            }
        }

        return $slowest;
    }

    public function count(): int
    {
        return count($this->slowTests);
    }
}
