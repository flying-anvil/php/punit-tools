<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\ReportFormatter;

use FlyingAnvil\Libfa\DataObject\Color\Color;
use FlyingAnvil\Libfa\DataObject\Color\ColorRamp;
use FlyingAnvil\Libfa\DataObject\Color\ColorRampStep;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\ColorableOutput;
use FlyingAnvil\Libfa\Utils\Console\OutputElement\DecorableOutput;
use FlyingAnvil\PunitTools\SlowTest\SlowReport;
use FlyingAnvil\PunitTools\SlowTest\SlowTestOptions;

class DefaultReportFormatter implements ReportFormatterInterface
{
    private const COLOR_SLOW = '#960417';
    private const COLOR_FAST = '#B6BC0D';

    private SlowTestOptions $options;

    public function __construct()
    {
    }

    public function format(SlowReport $report, SlowTestOptions $options): void
    {
        $this->options = $options;

        $slowCount   = $report->count();
        $slowestTest = $report->getSlowestTest();

        echo PHP_EOL, PHP_EOL;

        if ($slowCount === 0 || $slowestTest === null) {
            $this->printColorTextLn(sprintf(
                'No slow tests present (> %d ms (default))',
                $options->getDefaultSlowThreshold(),
            ), Color::create(35, 175, 50));

            return;
        }

        $displayCount = min($slowCount, $options->getReportLength());

        $this->printColorTextLn(sprintf(
            'Showing %d of %d slow tests (> %d ms (default))',
            $displayCount,
            $slowCount,
            $options->getDefaultSlowThreshold(),
        ));

        $slowTests = $report->getSlowTestsSorted();
        $padLengthCount    = strlen((string)$displayCount);
        $padLengthDuration = strlen((string)(int)$slowestTest->getTestDuration());

        $colorRamp = ColorRamp::create(
            ColorRampStep::create(0.0, Color::createFromHexString(self::COLOR_SLOW)),
            ColorRampStep::create(1.0, Color::createFromHexString(self::COLOR_FAST)),
        );

        $maxIndex = $displayCount - 1;
        foreach ($slowTests as $index => $slowTest) {
            if ($index > $maxIndex) {
                break;
            }

            $delimiter = match ($index) {
                0         => '┌',
                default   => '├',
                $maxIndex => '└',
            };

            /** @noinspection DisconnectedForeachInstructionInspection */
            if ($maxIndex === 0) {
                $delimiter = '╶';
            }

            if ($options->getUseColors()) {
                $percentage = $index / max($slowCount - 1, 1); // max to prevent division by 0
//                $percentage = ($slowTest->getTestDuration() - $options->getDefaultSlowThreshold()) / ($slowestTest->getTestDuration() - $options->getDefaultSlowThreshold());
//                $percentage = ($slowTest->getTestDuration() - $slowTest->getSlowThreshold()) / ($slowestTest->getTestDuration() - $slowestTest->getSlowThreshold());

                $delimiter = ColorableOutput::create($delimiter, $colorRamp->evaluate($percentage));
            }

            $this->printColorTextLn(sprintf(
                '%s: %s ms %s %s',
                str_pad((string)($index + 1), $padLengthCount, ' ', STR_PAD_LEFT),
                str_pad((string)(int)$slowTest->getTestDuration(), $padLengthDuration, ' ', STR_PAD_LEFT),
                $delimiter,
                $slowTest->toString(),
            ));
        }
    }

    private function printColorText(string $text, ?Color $color = null): void
    {
        if (!$color || !$this->options->getUseColors()) {
            echo $text;
            return;
        }

        echo DecorableOutput::create($text)->setForegroundColor($color);
    }

    private function printColorTextLn(string $text, ?Color $color = null): void
    {
        $this->printColorText($text, $color);
        echo PHP_EOL;
    }
}
