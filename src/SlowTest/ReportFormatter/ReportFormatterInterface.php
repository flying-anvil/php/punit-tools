<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\SlowTest\ReportFormatter;

use FlyingAnvil\PunitTools\SlowTest\SlowReport;
use FlyingAnvil\PunitTools\SlowTest\SlowTestOptions;

interface ReportFormatterInterface
{
    public function format(SlowReport $report, SlowTestOptions $options): void;
}
