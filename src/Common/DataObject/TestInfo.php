<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\Common\DataObject;

use FlyingAnvil\Libfa\Conversion\StringValue;
use FlyingAnvil\PunitTools\Exception\SlowTestException;
use Stringable;

class TestInfo implements StringValue, Stringable
{
    private string $shortTestClass;

    public function __construct(
        private string $fullTestClass,
        private string $testName,
        private ?int $dataSetNumber,
        private ?string $dataSetDescription,
    ) {
        $pos = strrpos($this->fullTestClass, '\\');
        $this->shortTestClass = substr($this->fullTestClass, $pos + 1);
    }

    public static function createFromRaw(string $rawTestName): self
    {
        $pattern = '/([a-zA-Z_][a-zA-Z0-9\\\\]+)::([a-zA-Z_][a-zA-Z_0-9\\\\]+)( with data set #(\d+) \((.+)\))?/';

        if (!preg_match($pattern, $rawTestName, $matches)) {
            // Happens when it's something like "Error" (when a dataset is invalid)
            throw new SlowTestException(sprintf(
                'Could not parse raw test name "%s"',
                $rawTestName,
            ));
        }

        [, $testClass, $testName] = $matches;
        $dataSetNumber      = isset($matches[4]) ? (int)$matches[4] : null;
        $dataSetDescription = $matches[5] ?? null;

        return new self($testClass, $testName, $dataSetNumber, $dataSetDescription);
    }

    public function getFullTestClass(): string
    {
        return $this->fullTestClass;
    }

    public function getShortTestClass(): string
    {
        return $this->shortTestClass;
    }

    public function getTestName(): string
    {
        return $this->testName;
    }

    public function hasDataSet(): bool
    {
        return $this->dataSetNumber !== null;
    }

    public function getDataSetNumber(): ?int
    {
        return $this->dataSetNumber;
    }

    public function getDataSetDescription(): ?string
    {
        return $this->dataSetDescription;
    }

    public function toString(): string
    {
        $dataSetString = $this->hasDataSet() ? sprintf(
            ' #%d',
            $this->dataSetNumber,
        ) : '';

        return sprintf(
            '%s::%s%s',
            $this->fullTestClass,
            $this->testName,
            $dataSetString,
        );
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
