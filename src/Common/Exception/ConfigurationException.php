<?php

declare(strict_types=1);

namespace FlyingAnvil\PunitTools\Common\Exception;

use FlyingAnvil\PunitTools\SlowTest\Exception\PunitToolsException;
use Throwable;

class ConfigurationException extends PunitToolsException
{
    public function __construct(string $hook, string $configurationField, string $message, ?Throwable $previous = null)
    {
        $betterMessage = sprintf(
            'Configuration error for %s: %s: %s',
            $hook,
            $configurationField,
            $message,
        );

        parent::__construct($betterMessage, previous: $previous);
    }
}
