# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

---

## [Unreleased]

- _nothing_

---

## [0.1.4] - 2024-09-11

### Changed
- Update libfa to "stable" version

[0.1.4]: https://gitlab.com/flying-anvil/php/punit-tools/-/compare/0.1.3...0.1.4

---

## [0.1.3] - 2022-12-01

### Fixed
- Removed `die` when parsing the test name fails
- Possible exceptions in `executeAfterTest` are caught now

[0.1.3]: https://gitlab.com/flying-anvil/php/punit-tools/-/compare/0.1.2...0.1.3

---

## [0.1.2] - 2022-01-17

### Changed
- Renamed `SlowTestHook` to `SlowTestReport`

### Fixed
- Fixed display error and crash when no tests are slow

[0.1.2]: https://gitlab.com/flying-anvil/php/punit-tools/-/compare/0.1.1...0.1.2

---

## [0.1.1] - 2022-01-17

### Fixed
- Fixed division by zero error in DefaultReportFormatter when there was only one slow test

[0.1.1]: https://gitlab.com/flying-anvil/php/punit-tools/-/compare/0.1.0...0.1.1

---

## [0.1.0] - 2022-01-14

### Added
- SlowTestHook for a report of slow tests

[0.1.0]: https://gitlab.com/flying-anvil/php/punit-tools/-/tags/0.1.0
