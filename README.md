# Punit-Tools

Extensions for PHPUnit

## Installation

`composer require --dev flying-anvil/punit-tools`

## Features

| Feature        | Description                                                                                                                                                                            |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SlowTestReport | Keeps track how long a test takes and gives a report of the slowest ones<br/>You can define thresholds for slow tests, either globally, for a whole test-class or a single test method |

## Slow Test

### Configuration

```xml
<extensions>
  <extension class="\FlyingAnvil\PunitTools\SlowTestReport">
    <arguments>
      <!-- default threshold (in ms) -->
      <integer>100</integer>
      <!-- report length -->
      <integer>10</integer>
      <!-- report formatter (default | FQCN) -->
      <string>default</string>
      <array>
        <element key="useColors">
          <string>auto</string> <!-- always | never | auto -->
        </element>
      </array>
    </arguments>
  </extension>
</extensions>
```

#### Report Formatter

You can define a custom report formatter by specifying its FQCN.
It must implement `\FlyingAnvil\PunitTools\SlowTest\ReportFormatter\ReportFormatterInterface`
and must be instantiable via `new` without parameters. 

#### Colors

When using `auto`, colors are used if running in an interactive session,
and if piping the output or when running tests automated they are not used.

#### SlowThreshold Attribute

Use this attribute to overwrite the threshold for a single test or test class.
Its value is the new threshold in ms.

```php
#[SlowThreshold(1100)]
public function testThatIsExpectedToBeSlow(): void
{
    sleep(1);
}

```
